#!/usr/bin/env bash

hereDir=$(dirname "${0}" | while read -r a; do cd "${a}" && pwd && break; done )
repoDir=$(readlink --canonicalize "${hereDir}/../../..")

lineNumber=${1}
processPath=${2}
processPid=${3}
sourceScript=${4}
startDateAsS=${5}

ellapsedSeconds=`${repoDir}/scripts/ellapsed_seconds/index.bash \
	"${processPath}" \
	"${startDateAsS}"
`

source=`echo ${sourceScript} | sed 's/\.\./ /g' | rev | cut -d' ' -f1 | rev | sed 's@^\./@@g'`":${lineNumber}"
repoDirLength=$(echo "${repoDir}" | wc --char)
relativeSource=${source:${repoDirLength}}

echo "[${ellapsedSeconds}][${relativeSource}]${processPath} (${processPid}->$PPID) <"
