#!/usr/bin/env bash

hereDir=$(dirname "${0}" | while read -r a; do cd "${a}" && pwd && break; done )
repoDir=$(readlink --canonicalize "${hereDir}/../../..")

#region args
lineNumber=${1}
processPath=${2}
processPid=${3}
sourceScript=${4}
startDateAsS=${5}

valuePartList=${6}
valuePartEscapedList=`echo ${valuePartList} | sed 's@%@%%@g'`
#endregion

ellapsedSeconds=`${repoDir}/scripts/ellapsed_seconds/index.bash \
	"${processPath}" \
	"${startDateAsS}"
`

source=`echo ${sourceScript} | sed 's/\.\./ /g' | rev | cut -d' ' -f1 | rev | sed 's@^\./@@g'`":${lineNumber}"
repoDirLength=$(echo "${repoDir}" | wc --char)
relativeSource=${source:${repoDirLength}}

maxValuePartEscapedKeyLength="0"
OIFS="${IFS}"
IFS=';'
for valuePartEscaped in ${valuePartEscapedList} ; do
	valuePartEscaped=`echo ${valuePartEscaped} `
	valuePartEscapedKey=`printf ${valuePartEscaped} | cut -d'=' -f1`
	if [ ${#valuePartEscapedKey} -ge ${maxValuePartEscapedKeyLength} ]; then
		maxValuePartEscapedKeyLength="${#valuePartEscapedKey}"
	fi
done
for valuePartEscaped in ${valuePartEscapedList} ; do
	valuePartEscapedKey=`printf ${valuePartEscaped} | cut -d'=' -f1`
	valuePartEscapedKeyPadded=`printf "%-${maxValuePartEscapedKeyLength}s\n" ${valuePartEscapedKey}`
	valuePartEscapedValue=`printf ${valuePartEscaped} | cut -d'=' -f2`
	ellapsedSeconds=`${repoDir}/scripts/ellapsed_seconds/index.bash \
		"${processPath}" \
		"${startDateAsS}"
	`
	echo "[${ellapsedSeconds}][${relativeSource}]${processPath} (${processPid}->$PPID) . ${valuePartEscapedKeyPadded} ${valuePartEscapedValue}"
done
IFS="${OIFS}"
