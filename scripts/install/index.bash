#!/usr/bin/env bash

hereDir=$(dirname "${0}" | while read -r a; do cd "${a}" && pwd && break; done )
repoDir=$(readlink --canonicalize "${hereDir}/../..")

processPath="📜🕳️"
startDateAsS=$(date +%s)

"${repoDir}/scripts/log/enter/index.bash" "${LINENO}" "${processPath}" "${PPID}" "${0}" "${startDateAsS}" \ "
	=
"

cd ${repoDir} && yarn install

"${repoDir}/scripts/log/exit/index.bash" "${LINENO}" "${processPath}" "${PPID}" "${0}" "${startDateAsS}"
