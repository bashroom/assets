#!/usr/bin/env bash

hereDir=$(dirname "${0}" | while read -r a; do cd "${a}" && pwd && break; done )
repoDir=$(readlink --canonicalize "${hereDir}/../../../..")

processPath="📜🐺🎣💬"

cd ${repoDir} && yarn run commitlint -E "HUSKY_GIT_PARAMS"
