#!/usr/bin/env bash

hereDir=$(dirname "${0}" | while read -r a; do cd "${a}" && pwd && break; done )
repoDir=$(readlink --canonicalize "${hereDir}/../..")

processPath="📜📄"
startDateAsS=$(date +%s)

branchName=${1}
baseUrl=${2:-"https://bashroom.gitlab.io/assets"}

"${repoDir}/scripts/log/enter/index.bash" "${LINENO}" "${processPath}" "${PPID}" "${0}" "${startDateAsS}" \ "
	branchName=${branchName};baseUrl=${baseUrl}
"

pwdResult=$(pwd)
echo "pwdResult="
echo ${pwdResult}

whoamiResult=$(whoami)
echo "whoamiResult="
echo ${whoamiResult}

lsResult=$(ls)
echo "lsResult="
echo ${lsResult}

if [ "${branchName}X" = "X" ] ; then
	"${repoDir}/scripts/log/info/index.bash" "${LINENO}" "${processPath}" "${PPID}" "${0}" "${startDateAsS}" \ "
		💥=A branch name is required as first argument
	"
else
	if [ "${branchName}X" = "developX" ] ; then
		branchUrlSegment="develop"
	else
		if [ "${branchName}X" = "masterX" ] ; then
			branchUrlSegment="master"
		else
			issueId=$(echo "${branchName}" | cut --delimiter='/' --fields=2 | cut --delimiter='_' --fields=1)

			if [ "${issueId}X" = "X" ] ; then
				"${repoDir}/scripts/log/info/index.bash" "${LINENO}" "${processPath}" "${PPID}" "${0}" "${startDateAsS}" \ "
					💥=issueId is empty, probably because it was not correctly parsed from branch ${branchName}
				"

				exit 2
			else
				branchUrlSegment="${issueId}"
			fi
		fi
	fi

	"${repoDir}/scripts/log/info/index.bash" "${LINENO}" "${processPath}" "${PPID}" "${0}" "${startDateAsS}" \ "
		branchUrlSegment=${branchUrlSegment}
	"

	rm --force --recursive ${repoDir}/public/${branchUrlSegment}

	mkdir --parents ${repoDir}/public/${branchUrlSegment}

	if [ "${branchUrlSegment}X" = "X" ] ; then
		"${repoDir}/scripts/log/info/index.bash" "${LINENO}" "${processPath}" "${PPID}" "${0}" "${startDateAsS}" \ "
			💥=The issue ID identification failed from parsing branchName ${branchName} (branchUrlSegment is empty)
		"
	else
		cp --recursive --no-clobber ${repoDir}/assets/* ${repoDir}/public/${branchUrlSegment}/

		assetFileRelativePathList=$(echo $(yarn run tree -a -i -l 6 --fullpath --base=${repoDir}/public/${branchUrlSegment} | grep -e "\.png" -e "\.svg" | sed 's/^[ \t]*//g' | sed "s@${repoDir}/public/${branchUrlSegment}/@@g"))

		for assetFileRelativePath in ${assetFileRelativePathList} ; do
			"${repoDir}/scripts/log/info/index.bash" "${LINENO}" "${processPath}" "${PPID}" "${0}" "${startDateAsS}" \ "
				url=https://bashroom.gitlab.io/assets/${branchUrlSegment}/${assetFileRelativePath}
			"
		done
	fi
fi

"${repoDir}/scripts/log/exit/index.bash" "${LINENO}" "${processPath}" "${PPID}" "${0}" "${startDateAsS}"
