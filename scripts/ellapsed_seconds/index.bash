#!/usr/bin/env bash

hereDir=$(dirname "${0}" | while read -r a; do cd "${a}" && pwd && break; done )
repoDir=$(readlink --canonicalize "${hereDir}/../..")

processPath="${1}   ⏱️"
startDateAsS=${2:-$(date +%s)}

${repoDir}/scripts/seconds-to-human/index.bash \
	"${processPath}" \
	"$(($(date +%s) - ${startDateAsS}))" \
	"${startDateAsS}"
