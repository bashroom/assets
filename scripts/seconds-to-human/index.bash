#!/usr/bin/env bash

hereDir=$(dirname "${0}" | while read -r a; do cd "${a}" && pwd && break; done )

processPath="${1}   👓"
seconds=${2}
startDateAsS=${3}

hoursPadded=`printf "%02d\n" $(( ${seconds} / 3600 ))`
minutesPadded=`printf "%02d\n" $(( (${seconds} / 60) % 60 ))`
secondsPadded=`printf "%02d\n" $(( ${seconds} % 60 ))`

echo "${hoursPadded}:${minutesPadded}:${secondsPadded}"
