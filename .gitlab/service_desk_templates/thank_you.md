Hello,
 
We have successfully received your message, so we created the ticket %{ISSUE_ID} in order to go ahead with it.

Be assured that we will get back to you as soon as possible.

Truly,

Bashroom - The support team
